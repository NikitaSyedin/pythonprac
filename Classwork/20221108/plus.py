class A:
	def __init__(self,val):
		self.val=val
	def __add__(self,other):
		return self.__class__(self.val+other.val)

class B(A):
	pass

c=B(10)+B(11)
print(c.val, type(c))